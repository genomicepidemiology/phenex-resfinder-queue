#!/usr/bin/env python3

import os
import sys
import pika
import time
import json
import argparse
import subprocess


def json_to_dict(json_str):
    return json.loads(json_str)


def dict_to_args_str(dictionary):
    args_str = ""
    for k, v in dictionary.items():
        if v is None:
            args_str += f"--{k} "
        else:
            args_str += f"--{k} {v} "
    return args_str.strip()


parser = argparse.ArgumentParser(description="RabbitMQ queue producer.")
parser.add_argument('-e', '--exchange', default=os.environ.get("SERVICE_NAME", "resfinder"), help='Name of rabbitmq exchagne where the messages will be send to.')
parser.add_argument('-q', '--queue', default=os.environ.get("SERVICE_VERSION", "v3"), help='Name of rabbitmq queue where the messages will be send to.')
args = parser.parse_args()

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit_broker'))

channel = connection.channel()
channel.exchange_declare(exchange=args.exchange, exchange_type='direct')
channel.queue_declare(args.queue, exclusive=False)
channel.queue_bind(exchange=args.exchange, queue=args.queue)

print('[*] Waiting for messages. To exit press CTRL+C')

# resfinder --inputfile /app/tests/test.fsa --methodPath /bin/blastn --threshold 0.90 --min_cov 0.60 --databasePath /app/db --outputPath /tmp/resfinder/results --databases aminoglycoside,beta-lactam -x
def callback(ch, method, properties, message):
    print(properties.content_type, properties.correlation_id)
    print("[x] Received %r" % message)

    msg = json_to_dict(message)
    args_str = dict_to_args_str(msg)
    command = f"{args.exchange} {args_str}"

    result = subprocess.run(command.split(" "), capture_output=True)
    print(result.stdout.replace(b'\n', b''))
    # r = json_to_dict(result.stdout)
    # print('rrrr', r)s

    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue=args.queue, auto_ack=False, on_message_callback=callback)
channel.start_consuming()
