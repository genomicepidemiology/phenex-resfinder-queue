#!/usr/bin/env python3

import sys
import pika
import json
import uuid
import argparse


def validate_json_str(content):
    try:
        json.loads(content)
    except ValueError as err:
        return False
    return True

parser = argparse.ArgumentParser(description="RabbitMQ queue producer.")
parser.add_argument('message', help='Message to be sent. Expected to be JSON formated string.')
parser.add_argument('-e', '--exchange', default='resfinder', help='Name of rabbitmq exchagne where the messages will be send to.')
parser.add_argument('-q', '--queue', default='v3', help='Name of rabbitmq queue where the messages will be send to.')
parser.add_argument('-id', '--correlation-id', default=str(uuid.uuid4()), help='Name of rabbitmq queue where the messages will be send to.')
args = parser.parse_args()

# '{"inputfile": "/app/tests/test.fsa", "methodPath": "/bin/blastn", "threshold": "0.90", "min_cov": "0.60", "databasePath": "/app/db", "outputPath": "/tmp/resfinder/results", "databases": "aminoglycoside,beta-lactam"}'
# '{"inputfile": "/app/tests/test.fsa", "methodPath": "/bin/blastn", "threshold": "0.90", "min_cov": "0.60", "databasePath": "/app/db", "outputPath": "/tmp/resfinder/results", "databases": "aminoglycoside,beta-lactam", "extented_output": null}'
if not validate_json_str(args.message):
    print("Error: message argumenr is invalid JSON string.", file=sys.stderr)
    exit(1)

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit_broker'))

channel = connection.channel()
channel.queue_declare(args.queue, exclusive=False)
channel.exchange_declare(exchange=args.exchange, exchange_type='direct')
channel.basic_publish(exchange=args.exchange, routing_key=args.queue, body=args.message, properties=pika.BasicProperties(
    delivery_mode=2,
    content_type='application/json',
    correlation_id=args.correlation_id
))

print(f"[x] Sent: '{args.message}' to '{args.queue}' queue by '{args.exchange}' exchange with correlation id '{args.correlation_id}'")

connection.close()
